import axios from 'axios'
import store from './store'
// import auth from './api/auth.js'

axios.interceptors.request.use(
    function (config) {

        // if(config.data) config.data.userId = auth.getUserId()
        store.commit('setLoadingState','open')

        return config;
    }, 
    function (error) {
        store.commit('setLoadingState','close')
        return Promise.reject(error);
    }
);
  
axios.interceptors.response.use(
    function (response) {
        store.commit('setLoadingState','close')
        return response;
    },
  
    function (error) {
        store.commit('setLoadingState','close')
        return Promise.reject(error);
    }
);