import Vue from 'vue'
import Vuex from 'vuex'
// import axios from 'axios'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    // serverMode : 'LOCAL',
    // BASE_URL_LOCAL : 'https://192.168.0.111:8443/pms/nxui_pms/index.html',
    // BASE_URL_PROD : 'http://192.168.0.8/pms/nxui_pms/goLogin.do',
    user : {
      userId : ''
    },
    loading : 'close'
  },

  mutations:{
    setLoadingState(state,loading){
      state.loading = loading
    }
  },

  actions:{
    setUser({ commit },{ loading }){
      commit('setLoadingState',loading)
    }
  },

  getters:{
    getLoadingState (state) {
      return `${state.loading}`
    }
  }
})